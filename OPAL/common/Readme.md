# Overview
***Opal Common*** implements generally useful data-structures and algorithms when writing static
analyses. *OPAL Common* in particular implements a framework for the concurrent execution of
fixed-point computations (see _fpcf_ package for more information).

## Remarks
This project contains (among others) macros.
Hence, it need to be compiled/need to be available before other projects can be compiled.
